#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bpe.params import searchParams2


def searchSessionParams(self, errorExit):
    vals = (
        ("isEffectOn", "m_isEffectOn", 4),
        ("m_monsterSnapOn_Skill", "m_monsterSnapOn_Skill", 4),
        ("m_monsterSnapOn_NoSkill", "m_monsterSnapOn_NoSkill", 4),
        ("m_isItemSnap", "m_isItemSnap", 4),
        ("m_isShowWhisperWnd", "m_isShowWhisperWnd", 4),
        ("m_isShowWhisperWnd_Friend", "m_isShowWhisperWnd_Friend", 4),
        ("m_isPlayWhisperOpenSound", "m_isPlayWhisperOpenSound", 4),
        ("m_bMakeMissEffect", "m_bMakeMissEffect", 4),
        ("m_bAutoOpenDetailWindowIfLowMemory",
         "m_bAutoOpenDetailWindowIfLowMemory", 4),
        ("onHoUserAI", "m_onHoUserAI", 4),
        ("onMerUserAI", "m_onMerUserAI", 4),
        ("m_isNoCtrl", "m_isNoCtrl", 4),
        ("m_isSimpleSkillWnd", "m_isSimpleSkillWnd", 4),
        ("m_bShowSkillDescript", "m_bShowSkillDescript", 4),
        ("m_bShowBattleFieldIcon", "m_bShowBattleFieldIcon", 4),
        ("m_bLockMouse", "m_bLockMouse", 4),
        ("FOG", "m_fogOn", 4),
        ("bgmIsPaused", "m_bgmIsPaused", 4),
    )
    self.addStruct("CSession")
    from bpe.blocks.sessionparams import blocks
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           True,
                           2)
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1], arr[val0], val[2], True)

    vals = (
        ("MEMORIALDUNWND.X", "m_MEMORIALDUNWND_X", 4),
        ("MEMORIALDUNWND.Y", "m_MEMORIALDUNWND_Y", 4),
    )
    self.addStruct("CSession")
    from bpe.blocks.sessionparams2 import blocks
    arr, _ = searchParams2(self,
                           vals,
                           blocks,
                           False,
                           "",
                           True,
                           2)
    for val in vals:
        val0 = val[0]
        if val0 in arr:
            self.addStructMember(val[1],
                                 arr[val0] - self.session,
                                 val[2],
                                 True)
