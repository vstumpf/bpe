#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSendEncryption(self, errorExit):
    if self.gCheatDefenderMgr == 0:
        return
    if self.sendPacket == 0:
        self.log("Error: sendPacket not found")
        if errorExit is True:
            exit(1)

    # 0  mov ecx, g_CCheatDefenderMgr
    # 6  push ebx
    # 7  push edi
    # 8  call CDClient_encryption
    # 13 cmp al, 1
    # 15 jz addr1
    # 17 cmp dword ptr [esi+m_socket], 0FFFFFFFFh
    # 21 jz addr1
    hookOffset = 7
    hookExitOffset = 8
    callEncryptionOffset = 9
    m_socketOffset = 19
    code = (
        "\x8B\x0D\xAB\xAB\xAB\xAB" +
        "\xAB" +
        "\x57" +
        "\xE8\xAB\xAB\xAB\xAB" +
        "\x3C\x01" +
        "\x74\xAB" +
        "\x83\xAB\xAB\xFF" +
        "\x74\xAB")
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.sendPacket,
                                   self.sendPacket + 0x50)
    if offset is False:
        self.log("Error: CDClient_encryption not found")
        if errorExit is True:
            exit(1)
    self.sendEncryption = self.getAddr(offset,
                                       callEncryptionOffset,
                                       callEncryptionOffset + 4)
    self.addRawFunc("CDClient_encryption",
                    self.sendEncryption)
    self.hookSendEncryptionExit = self.exe.rawToVa(offset) + \
        hookExitOffset
    self.hookSendEncryptionStartRaw = offset + hookOffset
    self.hookSendEncryptionStart = self.exe.rawToVa(
        self.hookSendEncryptionStartRaw)
    self.showVaAddr("SendEncryptionHook",
                    self.hookSendEncryptionStart)
    m_socket = self.exe.readUByte(offset + m_socketOffset)
    self.addStruct("CRagConnection")
    self.addStructMember("m_socket", m_socket, 4, True)
