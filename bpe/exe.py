#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2011-2013 Yommys
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import os
import struct

from bpe.cache import Cache
from bpe.section import Section, SectionRange

try:
    xrange
except NameError:
    xrange = range

unpackI = struct.Struct('I').unpack_from
unpackI2 = struct.Struct('II').unpack_from
unpackI4 = struct.Struct('IIII').unpack_from
unpacki4 = struct.Struct('iiii').unpack_from
unpackq = struct.Struct('q').unpack_from
unpacki = struct.Struct('i').unpack_from
unpackH = struct.Struct('H').unpack_from
unpackh = struct.Struct('h').unpack_from
unpackB = struct.Struct('B').unpack_from
unpackb = struct.Struct('b').unpack_from
packB = struct.Struct('B').pack_into
packH = struct.Struct('H').pack_into
packI = struct.Struct('I').pack_into


class Exe:
    def __init__(self):
        self.exe = None
        self.size = 0
        self.PEHeader = False
        self.imageBase = 0
        self.client_date = 0
        self.sectionNames = []
        self.sections = dict()
        self.subSections = []
        self.themida = False
        self.fileName = ""
        self.logFile = None
        self.outDir = ""
        self.asprotect = False
        self.fastSearch = False
        self.module = None
        self.rOffsetMin = -1
        self.rOffsetMax = -1
        self.vOffsetMin = -1
        self.vOffsetMax = -1
        self.initMatch()


    def initMatch(self):
        try:
            from bpe.module import fastsearch
            if fastsearch.version() >= "00000007":
                self.fastSearch = True
                self.module = fastsearch
                self.readUInt = fastsearch.readUInt
                self.matchWildcard = fastsearch.matchWildcard
#                self.match = fastsearch.match
                self.getAddr = fastsearch.getAddr
                print("Fast search")
            else:
                print("Slow search")
        except ImportError:
            print("Slow search")


    def log(self, text, show=True):
        self.logFile.write(text + "\n")
        if show is True:
            print(text)


    def loadSections(self):
        self.sections = dict()
        self.sectionNames = []
        self.rOffsetMin = -1
        self.rOffsetMax = -1
        self.vOffsetMin = -1
        self.vOffsetMax = -1
        sectionCount = self.read(self.PEHeader + 0x6, 2, "S")
        self.sectionCount = sectionCount
        curSection = self.sectionsHeader
        for idx in xrange(0, sectionCount):
            section = Section()
            section.offset = curSection
            section.realName = self.read(curSection, 8)
            section.name = section.realName.strip()
            if section.name.find("$") >= 0:
                self.log("Error: found grouped section: "
                         "{0}".format(section.name))
                exit(1)
            section.imageBase = self.imageBase
            while section.name[-1:] == '\x00':
                section.name = section.name[:-1]
            if section.name == "" or section.name is None:
                section.name = "sect_" + str(idx)
                self.themida = True
            if section.name == ".adata":
                self.log("Warning: found .adata section. " +
                         "Binary probably encoded.")
                self.asprotect = True
            self.log("Section {0}".format(section.name))
            section.vSize = self.readUInt(curSection + 8)
            section.vOffset = self.readUInt(curSection + 12)
            section.vEnd = section.vOffset + section.vSize
            self.log(" v: {0:<10}{1:<10}{2:<10}".format(hex(section.vOffset),
                                                        hex(section.vSize),
                                                        hex(section.vEnd)
                                                        ), False)
            section.rSize = self.readUInt(curSection + 16)
            section.realRSize = section.rSize
            section.rOffset = self.readUInt(curSection + 20)
            section.rEnd = section.rOffset + section.rSize
            sz = len(self.exe)
            if section.rSize > sz or section.rOffset + section.rSize > sz:
                self.log("Warning: wrong section size {0}".
                         format(section.name))
                section.rSize = sz - section.rOffset
            if section.rSize != section.realRSize:
                tmpStr = "{0} ({1}) ".format(hex(section.rSize),
                                             hex(section.realRSize))
            else:
                tmpStr = hex(section.rSize)
            self.log(" r: {0:<10}{1:<10}{2:<10}".format(hex(section.rOffset),
                                                        tmpStr,
                                                        hex(section.rEnd)
                                                        ), False)

            section.relocations = self.readUInt(curSection + 24)
            section.lineNumbers = self.readUInt(curSection + 28)
            section.relocationsCount = self.readUWord(curSection + 32)
            section.lineNumbersCount = self.readUWord(curSection + 34)
            section.flags = self.readUInt(curSection + 36)
            if section.relocations != 0:
                fmt = " relocs: {0}, {1}"
                self.log(fmt.format(hex(section.relocations),
                                    hex(section.relocationsCount)))
            if section.lineNumbers != 0:
                fmt = " lines: {0}, {1}"
                self.log(fmt.format(hex(section.lineNumbers),
                                    hex(section.lineNumbersCount)))
            section.vrDiff = section.vOffset - section.rOffset
            section.align = 0
            if section.rOffset > 0:
                if self.rOffsetMin == -1:
                    self.rOffsetMin = section.rOffset
                else:
                    if self.rOffsetMin > section.rOffset:
                        self.rOffsetMin = section.rOffset
            if self.rOffsetMax < section.rEnd:
                self.rOffsetMax = section.rEnd
            if self.vOffsetMin == -1:
                self.vOffsetMin = section.vOffset
            else:
                if self.vOffsetMin > section.vOffset:
                    self.vOffsetMin = section.vOffset
            if self.vOffsetMax < section.vEnd:
                self.vOffsetMax = section.vEnd
            section.init()
            section.addrRange = SectionRange(section)
            self.sections[section.name] = section
            self.sectionNames.append(section.name)
            curSection = curSection + 40
        self.log("v limits: {0} to {1}".format(hex(self.vOffsetMin),
                                               hex(self.vOffsetMax)))
        self.log("r limits: {0} to {1}".format(hex(self.rOffsetMin),
                                               hex(self.rOffsetMax)))
        self.initCodeSection()


    def loadOptionalInfo(self):
        self.subSections = []
        self.optHeader = self.PEHeader + 4 + 0x14
        self.optHeaderSize = self.readUWord(self.PEHeader + 4 + 16)
        if self.optHeaderSize == 0:
            self.log("Error: optional header size is zero")
            exit(1)
        self.sectionsHeader = self.optHeader + self.optHeaderSize
        self.log("optional header: {0}".format(hex(self.optHeader)))
        self.log("optional header size: {0}".format(hex(self.optHeaderSize)))
        if self.readUWord(self.optHeader) != 0x10b:
            self.log("Error: wrong option header signature")
            exit(1)
        self.linker = "{0}.{1}".format(self.readUByte(self.optHeader + 2),
                                       self.readUByte(self.optHeader + 3))
        self.log("linker: {0}".format(self.linker))
        self.codeSize = self.readUInt(self.optHeader + 4)
        self.log("Code size: {0}".format(hex(self.codeSize)))
        self.initCodeSize = self.readUInt(self.optHeader + 8)
        self.log("Code size (initialized): {0}".format(hex(self.initCodeSize)))
        self.uninitCodeSize = self.readUInt(self.optHeader + 12)
        self.log("Code size (uninitialized): "
                 "{0}".format(hex(self.uninitCodeSize)))
        self.entryPoint = self.readUInt(self.optHeader + 16)
        self.codeBase = self.readUInt(self.optHeader + 20)
        self.dataBase = self.readUInt(self.optHeader + 24)
        self.imageBase = self.readUInt(self.optHeader + 28)
        self.log("Code base: {0}".format(hex(self.codeBase)))
        self.log("Data base: {0}".format(hex(self.dataBase)))
        self.sectionAlignment = self.readUInt(self.optHeader + 32)
        self.log("Section alignment: {0}".format(hex(self.sectionAlignment)))
        self.fileAlignment = self.readUInt(self.optHeader + 36)
        self.log("File alignment: {0}".format(hex(self.fileAlignment)))
        self.imageSize = self.readUInt(self.optHeader + 56)
        self.log("Image size: {0}".format(hex(self.imageSize)))
        self.headersSize = self.readUInt(self.optHeader + 60)
        self.log("Headers size: {0}".format(hex(self.headersSize)))
        self.checkSum = self.readUInt(self.optHeader + 64)
        self.log("Checksum: {0}".format(hex(self.checkSum)))
        subSectionsCount = self.readUInt(self.optHeader + 92)
        curSub = self.optHeader + 96
        for cnt in xrange(0, subSectionsCount):
            subSection = (self.readUInt(curSub), self.readUInt(curSub + 4))
            self.subSections.append(subSection)
            curSub = curSub + 8
        for cnt in xrange(len(self.subSections), 16):
            self.subSections.append((0, 0))
        self.logSubSection(0, "Export table")
        self.logSubSection(1, "Import table")
        self.logSubSection(2, "Resource table")
        self.logSubSection(3, "Exception table")
        self.logSubSection(4, "Certificate table")
        self.logSubSection(5, "Relocation table")
        self.logSubSection(6, "Debug table")
        self.logSubSection(7, "Architecture table")
        self.logSubSection(8, "Global ptr table")
        self.logSubSection(9, "TLS table")
        self.logSubSection(10, "Load config table")
        self.logSubSection(11, "Bound import table")
        self.logSubSection(12, "IAT table")
        self.logSubSection(13, "Delay import table")
        self.logSubSection(14, "CLR header")
        self.logSubSection(15, "Reserved table")


    def logSubSection(self, id, name):
        section = self.subSections[id]
        if section[0] == 0:
            return
        self.log("{0}: {1}, size {2}".format(name,
                                             hex(section[0]),
                                             hex(section[1])))


    def postLoad(self):
        self.entryPoint = self.imageBase + self.entryPoint
        self.log("Entry point: {0}".format(hex(self.entryPoint)))
        if self.codeSize != self.codeSection.rSize:
            self.log("Warning: code size is not same with code section size: "
                     "{0} vs {1}".format(self.codeSize,
                                         self.codeSection.rSize))
        if self.codeBase != self.codeSection.vOffset:
            self.log("Warning: code base is not same with code section offset:"
                     " {0} vs {1}".format(self.codeBase,
                                          self.codeSection.vOffset))



    def loadClientDate(self):
        d = datetime.datetime.fromtimestamp(
            self.readUInt(self.PEHeader + 8))
        self.client_date = int(d.strftime("%Y")) * 10000 + \
            int(d.strftime("%m")) * 100 + int(d.strftime("%d"))


    def initExe(self):
        if self.fastSearch:
            self.module.setExe(self.exe)
        self.cache = Cache(self.fileName, self.size)


    def initCodeSection(self):
        if self.themida is True:
            self.codeSection = self.sections["sect_0"]
            self.rdataSection = self.sections["sect_0"]
            self.dataSection = self.sections["sect_0"]
        else:
            self.codeSection = self.sections[".text"]
            self.rdataSection = self.sections[".rdata"]
            self.dataSection = self.sections[".data"]
        self.rsrcSection = self.sections[".rsrc"]
        addr = self.subSections[1][0]
        if addr == 0:
            print("Error: import section not found")
            exit(1)
        _, section = self.vaToRawUnknownV(addr)
        if section is None:
            print("Error: import section with unknown address")
            exit(1)
        self.importSection = section
        codeSection = self.codeSection
        rawVaDiff = codeSection.rawVaDiff
        self.log("Code section: {0} to {1}".format(
                 hex(codeSection.rOffset - rawVaDiff),
                 hex(codeSection.rOffset + codeSection.rSize - rawVaDiff)))


    def detectPEHeader(self):
        # read PE header location from dos header
        self.PEHeader = self.readUInt(0x3c)
        # check for PE signature
        if self.readUInt(self.PEHeader) != 0x4550:
            self.log("Error: file is not PE or corrupted")
            exit(1)
        if self.PEHeader is False:
            self.log("Error: Cant find PE header. Exiting.")
            exit(1)

    def load(self, fileName, outFileName):
        outDir = "output/" + fileName
        with open("clients/" + fileName, "rb") as f:
            self.exe = f.read()
        self.size = len(self.exe)
        if fileName[-4:] == ".exe":
            fileName = fileName[:-4]
        self.fileName = fileName
        self.initExe()
        self.detectPEHeader()
        outDir = "output/" + fileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.logFile = open(outDir + "/" + outFileName, "wt")
        self.outDir = outDir
        self.loadClientDate()
        if self.client_date == 20041231:
            self.log("Broken client version detected. Fixing to correct one")
            self.client_date = 20120503
        self.log("client name: {0}".format(self.fileName))
        self.log("client date: {0}".format(self.client_date))
        self.loadOptionalInfo()
        self.loadSections()
        self.postLoad()
        return True


    def test(self, fileName, outFileName):
        outDir = "output/" + fileName
        with open("clients/" + fileName, "rb") as f:
            self.exe = f.read()
        if fileName[-4:] == ".exe":
            fileName = fileName[:-4]
        self.fileName = fileName
        self.size = len(self.exe)
        self.initExe()
        self.detectPEHeader()
        outDir = "output/" + fileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.logFile = open(outDir + "/" + outFileName, "wt")
        self.outDir = outDir
        self.imageBase = self.readUInt(self.PEHeader + 0x34)
        self.loadClientDate()
        self.log("exe name: {0}".format(self.fileName))
        for name in (".text", ".rdata", ".data", ".rsrc"):
            section = Section()
            section.name = name
            section.imageBase = 0
            section.vSize = self.size
            section.vOffset = 0
            section.vEnd = self.size
            section.rSize = self.size
            section.rOffset = 0
            section.rEnd = self.size
            section.vrDiff = 0
            section.align = 0
            self.sections[section.name] = section
            self.sectionNames.append(section.name)
            section.init()
        self.subSections.append((0, 4))
        self.subSections.append((1, 8))
        self.initCodeSection()
        return True


    def close(self):
        self.logFile.close()
        self.logFile = None
        self.sections = None
        self.exe = None
        self.imageBase = None


    def match(self, pattern, start, finish):
        if finish == -1:
            finish = self.size - len(pattern) + 1
        else:
            if finish > self.size - len(pattern) + 1:
                finish = self.size - len(pattern) + 1
        if start >= finish:
            return False
        # print "start - finish: {0} - {1}".format(start, finish)
        # print "search '{0}', from {1} to {2}".\
        #     format(pattern, start, finish + len(pattern) - 1)
        ret = self.exe.find(pattern, start, finish + len(pattern) - 1)
        if ret == -1:
            return False
        return ret


    def matchGen1(self, pattern, start, finish):
        for fpos in xrange(start, finish):
            yield fpos


    def matchGen2(self, pattern, start, finish):
        char = pattern[0]
        exe = self.exe
        findFunc = exe.find
        idx = findFunc(char, start, finish)
        while idx >= 0:
            # print "found fpos0:{0} = {1}".format(idx, char)
            yield idx
            start = idx + 1
            idx = findFunc(char, start, finish)


    def matchWildcard(self, pattern, wildcard, start, finish):
        exe = self.exe
        diff = self.size - len(pattern) + 1
        if finish > diff:
            finish = diff
        if start >= finish:
            return False
        gen = xrange(1, len(pattern))
        mGen = self.matchGen2(pattern, start, finish)
        for fpos in mGen:
            for ppos in gen:
                if exe[fpos + ppos] != pattern[ppos] and \
                   pattern[ppos] != wildcard:
                    break
            else:
                if fpos < 0:
                    return False
                return fpos
        return False


    def matchWildcardOffset(self, pattern, wildcard, start):
        sz = len(pattern)
        if start + sz >= self.size:
            return False
        exe = self.exe
        gen = xrange(0, sz)
        for ppos in gen:
            if exe[start + ppos] != pattern[ppos] and \
               pattern[ppos] != wildcard:
                return False
        return True


    def matches(self, pattern, start=0, finish=-1):
        offsets = []
        match = self.match
        sz = len(pattern)
        offset = match(pattern, start, finish)
        appendFunc = offsets.append
        while offset is not False:
            appendFunc(offset)
            offset = match(pattern,
                           offset + sz,
                           finish)
        if offsets:
            return offsets
        return False


    def matchesWildcard(self, pattern, wildcard, start=0, finish=-1):
        offsets = []
        offset = self.matchWildcard(pattern, wildcard, start, finish)
        appendFunc = offsets.append
        sz = len(pattern)
        while offset is not False:
            appendFunc(offset)
            offset = self.matchWildcard(pattern,
                                        wildcard,
                                        offset + sz,
                                        finish)
        if len(offsets) > 0:
            return offsets
        return False


    def read(self, offset, size, format=None):
        if offset >= self.size or self.size < 1:
            return False
        if format == "V" or format == "I":
            return unpackI(self.exe, offset)[0]
        elif format == "i":
            return unpacki(self.exe, offset)[0]
        elif format == "S" or format == "H":
            return unpackH(self.exe, offset)[0]
        elif format == "B":
            return unpackB(self.exe, offset)[0]
        elif format == "b":
            return unpackb(self.exe, offset)[0]
        elif format is not None:
            self.log("Error: wrong read format: {0}".format(format))
            exit(1)
        return self.exe[offset:offset + size]


    def readUInt(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpackI(self.exe, offset)[0]


    def readInt(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpacki(self.exe, offset)[0]


    def readQWord(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpackq(self.exe, offset)[0]


    def readUInt2(self, offset):
        if offset >= self.size or self.size < 8:
            return False
        return unpackI2(self.exe, offset)


    def readUInt4(self, offset):
        if offset >= self.size or self.size < 16:
            return False
        return unpackI4(self.exe, offset)


    def readInt4(self, offset):
        if offset >= self.size or self.size < 16:
            return False
        return unpacki4(self.exe, offset)


    def readByte(self, offset):
        if offset >= self.size or self.size < 1:
            return False
        return unpackb(self.exe, offset)[0]


    def readUByte(self, offset):
        if offset >= self.size or self.size < 1:
            return False
        return unpackB(self.exe, offset)[0]


    def readUWord(self, offset):
        if offset >= self.size or self.size < 2:
            return False
        return unpackH(self.exe, offset)[0]

    def readWord(self, offset):
        if offset >= self.size or self.size < 2:
            return False
        return unpackh(self.exe, offset)[0]


    def readStr(self, offset, strLen=1000):
        idx = self.exe.find('\x00', offset, offset + 1 + strLen)
        if idx is False or idx < 0:
            return False
        if idx >= offset and idx - offset < strLen:
            return self.exe[offset:idx]
        return None


    def getAddr(self, offset, addrOffset, instructionOffset):
        # ptr = self.readUInt(offset + addrOffset)
        # return (offset + instructionOffset + ptr) & 0xffffffff
        offset1 = offset + addrOffset
        if offset1 >= self.size or self.size < 4:
            return False
        return offset + instructionOffset + unpacki(self.exe, offset1)[0]


    def getAddrList(self, offset, addrOffsets):
        value1 = 0
        for addrOffset in addrOffsets:
            value2 = self.getAddr(offset,
                                  addrOffset,
                                  addrOffset + 4)
            if value1 == 0:
                value1 = value2
            elif value1 != value2:
                return False
        return value1


    def getVarAddr(self, offset, offset2):
        addr = offset + offset2[0]
        sz = offset2[1]
        if addr >= self.size or self.size < sz:
            return False
        if sz == 4:
            return unpacki(self.exe, addr)[0]
        elif sz == 1:
            return unpackb(self.exe, addr)[0]
        else:
            print("Error: unknown size {0} in getVarAddr".format(sz))


    def getVarAddrInt(self, offset):
        if offset >= self.size or self.size < 4:
            return False
        return unpacki(self.exe, offset)[0]


    def getVarAddrByte(self, offset):
        if offset >= self.size or self.size < 1:
            return False
        return unpackb(self.exe, offset)[0]


    def detectSection(self, code, cnt=1):
        for key in self.sectionNames:
            section = self.sections[key]
            print(section.name)
            offsets = self.matches(code,
                                   section.rOffset,
                                   section.rOffset + section.rSize)
            if offsets is False:
                print("code found 0 matches")
                continue
            if cnt != -1 and len(offsets) != cnt:
                print("code found {0} matches".format(len(offsets)))
                continue
            return section.name
        return False


    def codes(self, code, cnt=1, start=-1, finish=-1):
        section = self.codeSection
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offsets = self.matches(code,
                               start,
                               finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def anyCodes(self, code, cnt=1, start=-1, finish=-1):
        if start == -1:
            start = 0
        if finish == -1:
            finish = self.size
        offsets = self.matches(code,
                               start,
                               finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def codesWildcard(self, code, wildcard="", cnt=1, start=-1, finish=-1):
        section = self.codeSection
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offsets = self.matchesWildcard(code,
                                       wildcard,
                                       start,
                                       finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def anyCodesWildcard(self, code, wildcard="", cnt=1, start=-1, finish=-1):
        if start == -1:
            start = 0
        if finish == -1:
            finish = self.size
        offsets = self.matchesWildcard(code,
                                       wildcard,
                                       start,
                                       finish)
        if offsets is False:
            print("code found 0 matches")
            return False
        if cnt != -1 and len(offsets) != cnt:
            print("code found {0} matches".format(len(offsets)))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def code(self, code, start=-1, finish=-1):
        section = self.codeSection
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offset = self.match(code, start, finish)
        if offset is False:
            return False
        return offset


    def anyCode(self, code, start=-1, finish=-1):
        if start == -1:
            start = self.rOffsetMin
        if finish == -1:
            finish = self.rOffsetMax
        offset = self.match(code, start, finish)
        if offset is False:
            return False
        return offset


    def codeWildcard(self, code, wildcard, start=-1, finish=-1):
        section = self.codeSection
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offset = self.matchWildcard(code, wildcard, start, finish)
        if offset is False:
            return False
        return offset


    def anyCodeWildcard(self, code, wildcard, start=-1, finish=-1):
        if start == -1:
            start = self.rOffsetMin
        if finish == -1:
            finish = self.rOffsetMax
        offset = self.matchWildcard(code, wildcard, start, finish)
        if offset is False:
            return False
        return offset


    def stringBySectionName(self, code, name):
        if name in self.sectionNames:
            section = self.sections[name]
            offset = self.match("\x00" + code + "\x00",
                                section.rOffset,
                                section.rEnd)
            if offset is not False:
                return offset + 1, section
        return False, None


    def halfStringData(self, code):
        section = self.dataSection
        offset = self.match(code + "\x00",
                            section.rOffset,
                            section.rEnd)
        if offset is False and section != self.rdataSection:
            section = self.rdataSection
            offset = self.match(code + "\x00",
                                section.rOffset,
                                section.rEnd)
        if offset is not False:
            return offset
        return False


    def stringBySection(self, code, section):
        offset = self.match("\x00" + code + "\x00",
                            section.rOffset,
                            section.rEnd)
        if offset is not False:
            return offset + 1, section
        return False, None


    def string(self, code):
        codeSection = self.codeSection
        offset = self.match("\x00" + code + "\x00",
                            codeSection.rOffset,
                            codeSection.rEnd)
        if offset is not False:
            return offset + 1, codeSection
        offset, section = self.stringBySectionName(code, ".rdata")
        if offset is not False:
            return offset, section
        offset, section = self.stringBySectionName(code, ".data")
        if offset is not False:
            return offset, section
        for name in self.sectionNames:
            if name == ".rdata" or name == ".data" or name == codeSection.name:
                continue
            offset, section2 = self.stringBySectionName(code, name)
            if offset is not False:
                return offset, section2
        return False, None


    def stringData(self, code):
        section = self.dataSection
        offset = self.match("\x00" + code + "\x00",
                            section.rOffset,
                            section.rEnd)
        if offset is False and section != self.rdataSection:
            section = self.rdataSection
            offset = self.match("\x00" + code + "\x00",
                                section.rOffset,
                                section.rEnd)
        if offset is not False:
            return offset + 1
        return False


    def printRawAddr(self, text, offset):
        section = self.codeSection
        self.log(section.printRawAddr(text, offset))


    def rawToVaHex(self, offset):
        section = self.codeSection
        # return hex(section.rawToVa(offset))
        return hex(offset - section.rawVaDiff)


    def rawToVa(self, offset):
        section = self.codeSection
        # return section.rawToVa(offset)
        return offset - section.rawVaDiff


    def rawToVaUnknown(self, offset):
        sections = self.sections
        for key in sections:
            section = sections[key]
            if section.addrRange.isInR(offset):
                return section.rawToVa(offset), section
        return False, None


    def vaToRawUnknown(self, offset):
        sections = self.sections
        for key in sections:
            section = sections[key]
            if section.addrRange.isIn(offset):
                return section.vaToRaw(offset), section
        return False, None


    def vaToRawUnknownV(self, offset):
        sections = self.sections
        for key in sections:
            section = sections[key]
            if section.addrRange.isInV(offset):
                return section.vaToRaw(offset), section
        return False, None


    def getAddrSec(self, offset):
        section = self.codeSection
        return section.getAddrSec(offset)


    def toHex(self, addr, sz):
        addr = struct.pack("I", addr)
        return ("00" * (sz - len(addr))) + addr

    def toHexB(self, addr):
        addr = struct.pack("B", addr)
        return ("00" * (1 - len(addr))) + addr


    def isClientTooOld(self):
        return self.client_date <= 20100824


    def isClientSkipFlag(self):
        return self.client_date >= 20100817


    def getRawAddrString(self, addr):
        vaAddr, section = self.rawToVaUnknown(addr)
        if vaAddr is not False:
            return "{0}:{1} (va {2})".format(section.name,
                                             hex(addr),
                                             hex(vaAddr))
        else:
            return "{0} (va unknown)".format(hex(addr))


    def getRawAddrArr(self, arr):
        if isinstance(arr, list) is False and isinstance(arr, tuple) is False:
            return self.getRawAddrString(arr)
        return [self.getRawAddrString(val) for val in arr]


    def getVaAddrString(self, addr):
        rawAddr, section = self.vaToRawUnknownV(addr)
        if rawAddr is not False:
            return "{0}:{1} (raw {2})".format(section.name,
                                              hex(addr),
                                              hex(rawAddr))
        else:
            return "{0} (raw unknown)".format(hex(addr))


    def getVaAddrArr(self, arr):
        if isinstance(arr, list) is False and isinstance(arr, tuple) is False:
            return self.getVaAddrString(arr)
        return [self.getVaAddrString(val) for val in arr]


    def save(self, outName):
        with open(outName, "wb") as f:
            f.write(self.exe)


    def writeUByte(self, offset, value):
        packB(self.bytesExe, offset, value)


    def writeUWord(self, offset, value):
        packH(self.bytesExe, offset, value)


    def writeUInt(self, offset, value):
        packI(self.bytesExe, offset, value)


    def writeString(self, offset, value):
        for ch in value:
            self.bytesExe[offset] = ch
            offset = offset + 1


    def editStart(self):
        self.bytesExe = bytearray(self.exe)


    def editStop(self):
        self.exe = str(self.bytesExe)
        self.bytesExe = None
        self.size = len(self.exe)


    def writeSectionHeader(self, section):
        curSection = section.offset
        sectionName = section.realName
        sz = len(sectionName)
        if sz < 8:
            sectionName = sectionName + ("\x00" * (8 - sz))
        self.writeString(curSection, sectionName)
        self.writeUInt(curSection + 8, section.vSize)
        self.writeUInt(curSection + 12, section.vOffset)
        self.writeUInt(curSection + 16, section.realRSize)
        self.writeUInt(curSection + 20, section.rOffset)
        self.writeUInt(curSection + 24, section.relocations)
        self.writeUInt(curSection + 28, section.lineNumbers)
        self.writeUWord(curSection + 32, section.relocationsCount)
        self.writeUWord(curSection + 34, section.lineNumbersCount)
        self.writeUInt(curSection + 36, section.flags)


    def insertBytes(self, offset, size):
        self.bytesExe[offset:offset] = "\x00" * size


    def reInit(self):
        self.initExe()
        self.loadOptionalInfo()
        self.loadSections()
        self.postLoad()


    def alignToSection(self, offset):
        sectionAlignment = self.sectionAlignment
        if offset % sectionAlignment != 0:
            offset = (int(offset / sectionAlignment) + 1) * sectionAlignment
        return offset


    def alignToFile(self, offset):
        fileAlignment = self.fileAlignment
        if offset % fileAlignment != 0:
            offset = (int(offset / fileAlignment) + 1) * fileAlignment
        return offset


    def calcCheckSum(self):
        self.writeUInt(self.optHeader + 64, 0)
        sz = len(self.bytesExe)
        sz2 = sz - sz % 4
        checkSum = 0
        for i in xrange(0, sz2, 4):
            dword = unpackI(self.bytesExe, i)[0]
            checkSum += dword
            if checkSum >= 2**32:
                checkSum = (checkSum & 0xffffffff) + (checkSum >> 32)
        if sz != sz2:
            diff = sz - sz2
            if diff == 1:
                dword = unpackB(self, self.bytesExe, i)
            elif diff == 2:
                dword = unpackH(self, self.bytesExe, i)
            elif diff == 3:
                dword = unpackI(self, self.bytesExe, i, i + 3)
            checkSum += dword
            if checkSum >= 2**32:
                checkSum = (checkSum & 0xffffffff) + (checkSum >> 32)

        checkSum = (checkSum & 0xffff) + (checkSum >> 16)
        checkSum = checkSum + (checkSum >> 16)
        checkSum = checkSum & 0xffff
        checkSum = checkSum + sz
        checkSum = checkSum & 0xffffffff
        self.writeUInt(self.optHeader + 64, checkSum)
