#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.common import loadClient


class code1_limits(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        code = ("\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 391)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 392)


    def test2(self):
        code = ("\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 392)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 392)


    def test3(self):
        code = ("\x2E\x74\x65\x78\x74\x00\x00")
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 10, 391)
        self.assertTrue(offset is False)


    def test4(self):
        code = ("\x2E\x74\x65\x78\x74\x00\x00")
        self.extractor.exe.exe = self.extractor.exe.exe[0:392]
        self.extractor.exe.size = 392
        self.extractor.exe.initExe()
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is False)


    def test5(self):
        code = ("\x2E\x74\x65\x78\x74\x00\x00")
        sz = 400
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 20)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 392)


    def test6(self):
        code = ("MZ")
        sz = 2
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual(code, self.extractor.exe.exe)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)


    def test7(self):
        code = ("Z")
        sz = 2
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("MZ", self.extractor.exe.exe)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)


    def test8(self):
        code = ("\x90")
        sz = 3
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("MZ\x90", self.extractor.exe.exe)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "AB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "AB", 0, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.code(code, 0, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "AB", 1, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.code(code, 1, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "AB", 2, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.code(code, 2, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "AB", 3, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 3, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "AB", 4, 500)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 4, 500)
        self.assertTrue(offset is False)


    def test9(self):
        code = ("M")
        sz = 1
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("M", self.extractor.exe.exe)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.code(code, 0, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 100)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.code(code, 0, 100)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1000)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1000)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.code(code, 0, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 0)
        self.assertTrue(offset is False)


    def test10(self):
        code = ("Z")
        sz = 1
        self.extractor.exe.exe = self.extractor.exe.exe[0:sz]
        self.extractor.exe.size = sz
        self.extractor.exe.initExe()
        self.assertEqual("M", self.extractor.exe.exe)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 100)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 100)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1000)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 1, 1000)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 0, 0)
        self.assertTrue(offset is False)
