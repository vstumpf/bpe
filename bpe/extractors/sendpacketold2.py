#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSendPacketOld2(self, errorExit):
    # 1. search for 20080311 and older clients
    if self.getPacketSizeFunction != 0 or self.packetVersion > "20041207":
        return
    if self.loginPollAddr == 0:
        self.log("Error: CLoginMode::PollNetworkStatus "
                 "not found before searchSendPacketOld")
        if errorExit is True:
            exit(1)
        return
    if self.instanceR != 0:
        self.log("Error:: CRagConnection::instanceR detected "
                 "before old SendPacket")
        if errorExit is True:
            exit(1)
        return

    # 2004-12-07
    # 0  push 1BFh
    # 5  mov ecx, offset g_instanceR
    # 10 mov [ebp+offset1], 1
    # 14 call CRagConnection::GetPacketSize
    # 19 push eax
    # 20 mov ecx, offset g_instanceR
    # 25 call CRagConnection::SendPacket
    # 30 jmp addr1
    instanceOffset1 = 6
    getPacketSizeOffset = 15
    instanceOffset2 = 21
    sendOffset = 26
    code = (
        "\x68\xBF\x01\x00\x00" +
        "\xB9\xAB\xAB\xAB\xAB" +
        "\xC6\x45\xAB\x01" +
        "\xE8\xAB\xAB\xAB\xAB" +
        "\x50" +
        "\xB9\xAB\xAB\xAB\xAB" +
        "\xE8\xAB\xAB\xAB\xAB" +
        "\xEB")
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.loginPollAddr,
                                   self.loginPollAddr + 0x500)
    if offset is False:
        self.log("failed searchSendPacketOld in step 1.")
        if errorExit is True:
            exit(1)
        return
    # compare is CRagConnection::instanceR real functions
    instanceR1 = self.exe.readUInt(offset + instanceOffset1)
    instanceR2 = self.exe.readUInt(offset + instanceOffset2)
    if instanceR1 != instanceR2:
        self.log("Error: different g_instanceR "
                 "found in old searchSendPacket.")
        if errorExit is True:
            exit(1)
        return
    self.g_instanceR = instanceR1
    self.addVaVar("g_instanceR", self.g_instanceR)
    sendPacket = self.getAddr(offset, sendOffset, sendOffset + 4)
    if self.sendPacket != 0:
        if sendPacket != self.sendPacket:
            self.log("Error: CRagConnection::SendPacket validation failed")
            exit(1)
    else:
        self.sendPacket = sendPacket
        self.sendPacketVa = self.exe.rawToVa(self.sendPacket)
        self.addRawFunc("CRagConnection::SendPacket",
                        self.sendPacket)
    self.getPacketSizeFunction = self.getAddr(offset,
                                              getPacketSizeOffset,
                                              getPacketSizeOffset + 4)
    self.addRawFunc("CRagConnection::GetPacketSize",
                    self.getPacketSizeFunction)
    return True
