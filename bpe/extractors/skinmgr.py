#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchSkinMgr(self, errorExit):
    offset, section = self.exe.string("m_bAutoOpenDetailWindowIfLowMemory")
    if offset is False:
        self.log("failed in search m_bAutoOpenDetailWindowIfLowMemory")
        exit(1)
        return
    strAddr = section.rawToVa(offset)
    # search in CSession::WriteOptionToRegistry
    # 2016 - 2018 +
    # 0  push 4
    # 2  push 0
    # 4  push offset aM_bautoopendet
    # 9  push [ebp+phkResult]
    # 15 call ebx
    # 17 mov ecx, offset g_skinMgr
    # 22 call CSkinMgr_GetCurrentSkinName
    # 27 push eax
    code = (
        "\x6A\x04"                           # 0
        "\x6A\x00"                           # 2
        "\x68" + self.exe.toHex(strAddr, 4) +  # 4
        "\xFF\xB5\xAB\xAB\xAB\xAB"           # 9
        "\xFF\xD3"                           # 15
        "\xB9\xAB\xAB\xAB\xAB"               # 17
        "\xE8\xAB\xAB\xAB\xFF"               # 22
        "\x50"                               # 27
    )
    skinMgrOffset = 18
    skinNameOffset = 23
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2014 - 2015
        # 0  push 4
        # 2  push 0
        # 4  mov ecx, [ebp+phkResult]
        # 10 push offset aM_bautoopendet
        # 15 push ecx
        # 16 call ebx
        # 18 mov ecx, offset g_skinMgr
        # 23 call CSkinMgr_GetCurrentSkinName
        # 28 push eax
        code = (
            "\x6A\x04"                        # 0
            "\x6A\x00"                        # 2
            "\x8B\x8D\xAB\xAB\xAB\xAB"        # 4
            "\x68" + self.exe.toHex(strAddr, 4) +  # 4
            "\x51"                            # 15
            "\xFF\xD3"                        # 16
            "\xB9\xAB\xAB\xAB\xAB"            # 18
            "\xE8\xAB\xAB\xAB\xFF"            # 23
            "\x50"                            # 28
        )
        skinMgrOffset = 19
        skinNameOffset = 24
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2008 - 2013
        # 0  push 4
        # 2  push 0
        # 4  push offset aM_bautoopendet
        # 9  push edx
        # 10 call ebx
        # 12 mov ecx, offset g_skinMgr
        # 17 call CSkinMgr_GetCurrentSkinName
        # 22 push eax
        code = (
            "\x6A\x04"                        # 0
            "\x6A\x00"                        # 2
            "\x68" + self.exe.toHex(strAddr, 4) +  # 4
            "\xAB"                            # 9
            "\xFF\xD3"                        # 10
            "\xB9\xAB\xAB\xAB\xAB"            # 12
            "\xE8\xAB\xAB\xAB\xFF"            # 17
            "\x50"                            # 22
        )
        skinMgrOffset = 13
        skinNameOffset = 18
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2007
        # 0  push 4
        # 2  mov ecx, [ebp+phkResult]
        # 5  push 0
        # 7  push offset aM_bautoopendet
        # 12 push ecx
        # 13 call ebx
        # 15 mov ecx, offset g_skinMgr
        # 20 call CSkinMgr_GetCurrentSkinName
        # 25 push eax
        code = (
            "\x6A\x04"                        # 0
            "\x8B\x4D\xAB"                    # 2
            "\x6A\x00"                        # 5
            "\x68" + self.exe.toHex(strAddr, 4) +  # 4
            "\x51"                            # 12
            "\xFF\xD3"                        # 13
            "\xB9\xAB\xAB\xAB\xAB"            # 15
            "\xE8\xAB\xAB\xAB\xFF"            # 20
            "\x50"                            # 25
        )
        skinMgrOffset = 16
        skinNameOffset = 21
        offset = self.exe.codeWildcard(code, "\xAB")

    if offset is False:
        self.log("failed in search SkinMgr.")
        if errorExit is True:
            exit(1)
        return

    self.gSkinMgr = self.exe.read(offset + skinMgrOffset, 4, "V")
    self.CSkinMgrGetCurrentSkinName = self.getAddr(offset,
                                                   skinNameOffset,
                                                   skinNameOffset + 4)
    self.addVaVar("g_skinMgr", self.gSkinMgr)
    self.addRawFunc("CSkinMgr::GetCurrentSkinName",
                    self.CSkinMgrGetCurrentSkinName)
