#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchShuffle0(self, errorExit):
    # seach first shuffle packet
    # 0  jmp addr1
    # 5  mov eax, packetId
    # 10 mov [ebp + addr2], ax
    # 17 lea eax, [ebp + addr3]
    # 23 push eax
    # 24 mov [ebp + addr4], bl
    # 30 mov [ebp + addr5], ecx
    # 36 push packetId
    # 41 jmp addr6
    code = (
        "\xE9\xAB\xAB\xFF\xFF" +
        "\xB8\xAB\xAB\x00\x00" +
        "\x66\xAB\xAB\xAB\xAB\xFF\xFF" +
        "\x8D\xAB\xAB\xAB\xFF\xFF" +
        "\x50" +
        "\x88\xAB\xAB\xAB\xFF\xFF" +
        "\x89\xAB\xAB\xAB\xFF\xFF" +
        "\x68\xAB\xAB\x00\x00" +
        "\xE9\xAB\xAB\x00\x00")
    shuffle0Offset1 = 6
    shuffle0Offset2 = 37
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2015-03-11
        # 0  jmp addr1
        # 5  mov cl, byte ptr [ebp + var1]
        # 11 mov edx, [ebp + var2]
        # 17 mov eax, packetId
        # 22 mov [ebp + addr2], ax
        # 29 lea eax, [ebp + addr3]
        # 35 push eax
        # 36 mov [ebp + addr4], cl
        # 42 push packetId
        # 47 jmp addr6
        # 52 mov eax, [ebp + var3]
        code = (
            "\xE9\xAB\xAB\x00\x00" +
            "\x8A\xAB\xAB\xAB\xFF\xFF" +
            "\x8B\xAB\xAB\xAB\xFF\xFF" +
            "\xB8\xAB\xAB\x00\x00" +
            "\x66\xAB\xAB\xAB\xAB\xFF\xFF" +
            "\x8D\xAB\xAB\xAB\xFF\xFF" +
            "\x50" +
            "\x88\xAB\xAB\xAB\xFF\xFF" +
            "\x68\xAB\xAB\x00\x00" +
            "\xE9\xAB\xAB\x00\x00" +
            "\x8B\xAB\xAB\xAB\xFF\xFF")
        shuffle0Offset1 = 18
        shuffle0Offset2 = 43
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        # 2015-06-24
        # 0  jmp addr1
        # 5  mov cl, byte ptr [ebp + var1]
        # 11 mov edx, [ebp + var2]
        # 17 mov eax, packetId
        # 22 mov [ebp + addr2], ax
        # 29 lea eax, [ebp + addr3]
        # 35 push eax
        # 36 mov [ebp + addr4], cl
        # 42 mov [ebp + addr5], edx
        # 48 push packetId
        # 53 jmp addr6
        # 58 mov eax, [ebp + var3]
        code = (
            "\xE9\xAB\xAB\x00\x00" +
            "\x8A\xAB\xAB\xAB\xFF\xFF" +
            "\x8B\xAB\xAB\xAB\xFF\xFF" +
            "\xB8\xAB\xAB\x00\x00" +
            "\x66\xAB\xAB\xAB\xAB\xFF\xFF" +
            "\x8D\xAB\xAB\xAB\xFF\xFF" +
            "\x50" +
            "\x88\xAB\xAB\xAB\xFF\xFF" +
            "\x89\xAB\xAB\xAB\xFF\xFF" +
            "\x68\xAB\xAB\x00\x00" +
            "\xE9\xAB\xAB\x00\x00" +
            "\x8B\xAB\xAB\xAB\xFF\xFF")
        shuffle0Offset1 = 18
        shuffle0Offset2 = 49
        offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.log("failed in search first shuffle packet")
        if errorExit is True:
            exit(1)
        return
    shuffle00 = self.exe.read(offset + shuffle0Offset1, 4, "V")
    shuffle01 = self.exe.read(offset + shuffle0Offset2, 4, "V")
    if shuffle00 != shuffle01:
        self.log("Error: found two different shuffle0 packets")
        exit(1)
    if shuffle00 < 0x100 or shuffle00 > 0xa00:
        self.log("Error: posisble wrong shyffle0: {0}".format(shuffle00))
        exit(1)
    self.shuffle0 = shuffle00
    self.log("Shuffle packet 0 (CZ_REQUEST_ACT): {0}".format(
        hex(self.shuffle0)))
