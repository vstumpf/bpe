#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.common import loadClient


class code1_ranges(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def testA(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("a")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testB(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("b")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testC(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("c")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testAB(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("ab")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testBC(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("bc")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 1)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testC1(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("c")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testC2(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("c")

        offset = self.extractor.exe.code(code, 0, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 0, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 1, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 1, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 1, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 1, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 1, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 2, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 2, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 3, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3, 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 2)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testABC(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("abc")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 0)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testN(self):
        self.extractor.exe.exe = "abcdefghijklmn\x00"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("n")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 11)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 11)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 12)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 13)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testMN(self):
        self.extractor.exe.exe = "abcdefghijklmn"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("mn")

        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 2)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 3)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 4)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 11)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 11)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 12)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 13)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 15)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 16)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 16)
        self.assertTrue(offset is False)


    def testMN2(self):
        self.extractor.exe.exe = "abcdefghijklmn"
        self.extractor.exe.size = len(self.extractor.exe.exe)
        self.extractor.exe.initExe()
        code = ("mn")

        offset = self.extractor.exe.code(code, 0, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 12)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 12)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 12)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 0, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 0, 14)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 14)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 0, 15)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 0, 15)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 1, 12)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 12)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 1, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 1, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 12, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 12, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 12, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 12, 12)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 12)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 12, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 13)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 12, 14)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 14)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 12, 15)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 12, 15)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 12)

        offset = self.extractor.exe.code(code, 13, 0)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 0)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 1)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 1)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 2)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 2)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 11)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 11)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 12)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 12)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 13)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 13)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 14)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 14)
        self.assertTrue(offset is False)

        offset = self.extractor.exe.code(code, 13, 15)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 13, 15)
        self.assertTrue(offset is False)
