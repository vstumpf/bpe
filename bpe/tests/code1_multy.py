#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest

from bpe.tests.common import loadClient


class code1_multy(unittest.TestCase):
    def setUp(self):
        self.extractor = loadClient("head.exe")


    def tearDown(self):
        self.extractor = None


    def test1(self):
        code = ("data")
        offset = self.extractor.exe.codeWildcard(code, "\xAB")
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.code(code)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 433, 436)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.code(code, 433, 436)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 433, 435)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.code(code, 433, 435)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 433, 434)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.code(code, 433, 434)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 433)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 433, 433)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 433, 433)
        self.assertTrue(offset is False)


    def test2(self):
        code = ("data")
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 434)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.code(code, 434)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 435, 475)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.code(code, 435, 475)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 435, 474)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 435, 474)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 474, 475)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.code(code, 474, 475)
        self.assertTrue(offset is not False)
        self.assertEqual(offset, 474)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 474, 474)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 474, 474)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.codeWildcard(code, "\xAB", 474, 473)
        self.assertTrue(offset is False)
        offset = self.extractor.exe.code(code, 474, 473)
        self.assertTrue(offset is False)
