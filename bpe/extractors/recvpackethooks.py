#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchRecvPacketHooks(self):
    if self.gCheatDefenderMgr == 0:
        return
    if self.recvPacket == 0:
        self.log("Error: recvPacket not set")
        exit(1)
    if self.hookRecvPacketStartMap == 0 or self.hookRecvPacketExitMap == 0:
        self.log("Error: recvPacket hooks not set")
        exit(1)


    # 1. search for all RecvPacket calls.
    # call CRagConnection_RecvPacket
    code = (
        "\xE8")
    offsets = self.exe.codes(code, -1)
    if offsets is False:
        self.log("Error: not found any calls")
        exit(1)
    recvPacketsAll = set()
    for offset in offsets:
        addr = self.getAddr(offset, 1, 5)
        if addr == self.recvPacket:
            recvPacketsAll.add(offset)
    # 2. check found number of calls
    if len(recvPacketsAll) != 3:
        self.log("Error: found wrong number of calls to recvPacket")
        exit(1)
    if self.callRecvPacket1 not in recvPacketsAll:
        self.log("Error: RecvPacket call in map server "
                 "not detected or wrong.")
        exit(1)
    recvPacketsAll.remove(self.callRecvPacket1)

    # 3. sarch for call in LoginPacketHandler

    # 0 mov ecx, eax
    # 2 call CRagConnection_RecvPacket
    code = (
        "\x8B\xC8" +
        "\xE8")
    offsets = self.exe.codes(code,
                             -1,
                             self.loginPollAddr,
                             self.loginPollAddr + 0x700)
    if offsets is False:
        self.log("Error: RecvPacket calls not found in login handler")
        exit(1)
    recvPacketsLogin = []
    for offset in offsets:
        addr = self.getAddr(offset, 3, 7)
        if addr == self.recvPacket:
            recvPacketsLogin.append(offset)
    if len(recvPacketsLogin) != 2:
        self.log("Error: wong number of RecvPacket "
                 "calls found in login handler")
        exit(1)
    for offset in recvPacketsLogin:
        if offset + 2 not in recvPacketsAll:
            self.log("Error: found wrong RecvPacket call in login handler")
            exit(1)

    self.hookRecvPacketStartLogin1 = self.exe.rawToVa(recvPacketsLogin[0])
    self.hookRecvPacketStartLogin1Raw = recvPacketsLogin[0]
    self.hookRecvPacketExitLogin1 = self.exe.rawToVa(recvPacketsLogin[0]) + 7
    self.showVaAddr("hookRecvPacketStartLogin1",
                    self.hookRecvPacketStartLogin1)
    self.showVaAddr("hookRecvPacketExitLogin1",
                    self.hookRecvPacketExitLogin1)
    self.hookRecvPacketStartLogin2 = self.exe.rawToVa(recvPacketsLogin[1])
    self.hookRecvPacketExitLogin2 = self.exe.rawToVa(recvPacketsLogin[1]) + 7
    self.showVaAddr("hookRecvPacketStartLogin2",
                    self.hookRecvPacketStartLogin2)
    self.showVaAddr("hookRecvPacketExitLogin2",
                    self.hookRecvPacketExitLogin2)
