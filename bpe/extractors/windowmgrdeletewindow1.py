#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchWindowMgrDeleteWindow1(self, errorExit):
    # search in UIWindowMgr_MakeWindow, case 0x22

    # 0  jz loc_547968
    # 6  push 45h
    # 8  mov ecx, esi
    # 10 call UIWindowMgr_DeleteWindow
    # 15 mov edi, [esi+2A4h]
    code = (
        "\x0F\x84\xAB\xAB\xAB\xAB"        # 0
        "\x6A\x45"                        # 6
        "\x8B\xCE"                        # 8
        "\xE8\xAB\xAB\xAB\xAB"            # 10
        "\x8B\xBE\xAB\xAB\x00\x00"        # 15
    )
    deleteWindowOffset = 11
    offset = self.exe.codeWildcard(code,
                                   "\xAB",
                                   self.UIWindowMgrMakeWindow,
                                   self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jz loc_6E2BE5
        # 6  push 45h
        # 8  mov ecx, ebx
        # 10 call UIWindowMgr_DeleteWindow
        # 15 mov edi, [ebx+UIWindowMgr.m_messengerGroupWnd]
        code = (
            "\x0F\x84\xAB\xAB\xAB\xAB"        # 0
            "\x6A\x45"                        # 6
            "\x8B\xCB"                        # 8
            "\xE8\xAB\xAB\xAB\xAB"            # 10
            "\x8B\xBB\xAB\xAB\x00\x00"        # 15
        )
        deleteWindowOffset = 11
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jz loc_5E4B88
        # 6  push 45h
        # 8  mov ecx, esi
        # 10 call UIWindowMgr_DeleteWindow
        # 15 cmp [esi+2D0h], edi
        code = (
            "\x0F\x84\xAB\xAB\xAB\xAB"        # 0
            "\x6A\x45"                        # 6
            "\x8B\xCE"                        # 8
            "\xE8\xAB\xAB\xAB\xAB"            # 10
            "\x39\xBE\xAB\xAB\x00\x00"        # 15
        )
        deleteWindowOffset = 11
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_5256F6
        # 2  push 45h
        # 4  mov ecx, esi
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov edi, [esi+2A4h]
        code = (
            "\xEB\xAB"                        # 0
            "\x6A\x45"                        # 2
            "\x8B\xCE"                        # 4
            "\xE8\xAB\xAB\xAB\xAB"            # 6
            "\x8B\xBE\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4FCF00
        # 5  push 45h
        # 7  mov ecx, esi
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov ebx, [esi+294h]
        code = (
            "\xE9\xAB\xAB\xAB\xAB"            # 0
            "\x6A\x45"                        # 5
            "\x8B\xCE"                        # 7
            "\xE8\xAB\xAB\xAB\xAB"            # 9
            "\x8B\x9E\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F6CB7
        # 5  push 45h
        # 7  call UIWindowMgr_DeleteWindow
        # 12 mov edi, [esi+124h]
        code = (
            "\xE9\xAB\xAB\x00\x00"            # 0
            "\x6A\x45"                        # 5
            "\xE8\xAB\xAB\xAB\xAB"            # 7
            "\x8B\xBE\xAB\xAB\x00\x00"        # 12
        )
        deleteWindowOffset = 8
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_4F88CB
        # 2  push 45h
        # 4  mov ecx, ebx
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov esi, [ebx+148h]
        code = (
            "\xEB\xAB"                        # 0
            "\x6A\x45"                        # 2
            "\x8B\xCB"                        # 4
            "\xE8\xAB\xAB\xAB\xAB"            # 6
            "\x8B\xB3\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F7B87
        # 5  push 45h
        # 7  mov ecx, ebx
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov esi, [ebx+14Ch]
        code = (
            "\xE9\xAB\xAB\xAB\xAB"            # 0
            "\x6A\x45"                        # 5
            "\x8B\xCB"                        # 7
            "\xE8\xAB\xAB\xAB\xAB"            # 9
            "\x8B\xB3\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp loc_4F1306
        # 5  push 45h
        # 7  mov ecx, esi
        # 9  call UIWindowMgr_DeleteWindow
        # 14 mov edi, [esi+130h]
        code = (
            "\xE9\xAB\xAB\xAB\xAB"            # 0
            "\x6A\x45"                        # 5
            "\x8B\xCE"                        # 7
            "\xE8\xAB\xAB\xAB\xAB"            # 9
            "\x8B\xBE\xAB\xAB\x00\x00"        # 14
        )
        deleteWindowOffset = 10
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        # 0  jmp short loc_5EF567
        # 2  push 45h
        # 4  mov ecx, ebx
        # 6  call UIWindowMgr_DeleteWindow
        # 11 mov edi, [ebx+288h]
        code = (
            "\xEB\xAB"                        # 0
            "\x6A\x45"                        # 2
            "\x8B\xCB"                        # 4
            "\xE8\xAB\xAB\xAB\xAB"            # 6
            "\x8B\xBB\xAB\xAB\x00\x00"        # 11
        )
        deleteWindowOffset = 7
        offset = self.exe.codeWildcard(code,
                                       "\xAB",
                                       self.UIWindowMgrMakeWindow,
                                       self.UIWindowMgrMakeWindow + 0x2000)

    if offset is False:
        self.log("failed in search UIWindowMgr::DeleteWindow.")
        if errorExit is True:
            exit(1)
        return

    self.UIWindowMgrDeleteWindow = self.getAddr(offset,
                                                deleteWindowOffset,
                                                deleteWindowOffset + 4)
    self.UIWindowMgrDeleteWindowVa = self.exe.rawToVa(
        self.UIWindowMgrDeleteWindow)
    self.addRawFunc("UIWindowMgr::DeleteWindow",
                    self.UIWindowMgrDeleteWindow)
