#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def searchGetGameMode(self):
    # 0  cmp [ecx+CModeMgr.m_curModeType], 1
    # 4  jnz short label1
    # 6  mov eax, [ecx+CModeMgr.m_curMode]
    # 9  retn
    # 10 label1: xor eax, eax
    # 12 retn
    curModeTypeOffset = 2
    curModeOffset = 8
    code = (
        "\x83\x79\xAB\x01" +
        "\x75\x04" +
        "\x8B\x41\xAB" +
        "\xC3" +
        "\x33\xC0" +
        "\xC3")
    offset = self.exe.codeWildcard(code, "\xAB")
    if offset is False:
        self.log("Error: failed in seach CModeMgr::GetGameMode")
        exit(1)
        return
    self.getGameModeAddr = offset
    self.getGameModeAddrVa = self.exe.rawToVa(offset)
    self.addRawFunc("CModeMgr::GetGameMode", self.getGameModeAddr)
    self.CModeMgr_m_curModeType = self.exe.read(offset + curModeTypeOffset,
                                                1,
                                                'B')
    self.CModeMgr_m_curMode = self.exe.read(offset + curModeOffset, 1, 'B')

    self.addStruct("CModeMgr")
    self.addStructMember("m_curModeType", self.CModeMgr_m_curModeType, 4, True)
    self.addStructMember("m_curMode", self.CModeMgr_m_curMode, 4, True)
